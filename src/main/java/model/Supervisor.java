package model;

public class Supervisor extends Empleado {

    public Supervisor (){
        super();
    }

    public Supervisor (String nombre, String apellido, Integer cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.tipoEmpleado = "supervisor";
        this.libre = true;
    }
}
