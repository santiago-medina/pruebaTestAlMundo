package model;

public class Operador extends Empleado {

    public Operador (){
        super();
    }

    public Operador (String nombre, String apellido, Integer cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.tipoEmpleado = "operador";
        this.libre = true;
    }
}
