package model;

public class Empleado {

    protected String nombre;
    protected String apellido;
    protected Integer cedula;
    protected String tipoEmpleado;
    protected Boolean libre;

    public Empleado(){

    }

    public Empleado(String nombre, String apellido, Integer cedula){

        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.tipoEmpleado = "empleado";

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getCedula() {
        return cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    public String getNombreCompleto(){
        return (nombre + " " + apellido);
    }

    public String getTipoEmpleado() {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(String tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }

    public Boolean getLibre() {
        return libre;
    }

    public void setLibre(Boolean libre) {
        this.libre = libre;
    }
}
