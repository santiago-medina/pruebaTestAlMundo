package model;

public class Director extends Empleado{

    public Director (){
        super();
    }

    public Director (String nombre, String apellido, Integer cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.tipoEmpleado = "director";
        this.libre = true;
    }
}
