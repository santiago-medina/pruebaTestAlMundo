package model;

import java.util.Random;

public class LLamada extends Thread {

    private Operador operador = null;
    private Supervisor supervisor = null;
    private Director director = null;

    public LLamada(){

    }

    public LLamada(Operador operador){
        this.operador = operador;
    }

    public LLamada(Supervisor supervisor){
        this.supervisor = supervisor;
    }

    public LLamada(Director director){
        this.director = director;
    }


    @Override
    public void run() {
        super.run();
        int numeroAleatorio = (int) (Math.random()*5+5);
        numeroAleatorio = numeroAleatorio*1000;
        if(operador!=null){
            System.out.println("el operador " + operador.getNombreCompleto() + " atendera su llamada");
            try {
                System.out.println("operador " + operador.getNombreCompleto() + " en llamada durante: " + numeroAleatorio);
                Thread.sleep(numeroAleatorio);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            operador.setLibre(true);
            System.out.println("el operador " + operador.getNombreCompleto() + " se encuentra disponible");
        }
        if(supervisor!=null){
            System.out.println("el supervisor " + supervisor.getNombreCompleto() + " atendera su llamada");
            try {
                System.out.println("supervisor " + supervisor.getNombreCompleto() + " en llamada durante: " + numeroAleatorio);
                Thread.sleep(numeroAleatorio);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            supervisor.setLibre(true);
            System.out.println("el supervisor " + supervisor.getNombreCompleto() + " se encuentra disponible");
        }

        if(director!=null){
            System.out.println("el director " + director.getNombreCompleto() + " atendera su llamada");
            try {
                System.out.println("director " + director.getNombreCompleto() + " en llamada durante: " + numeroAleatorio);
                Thread.sleep(numeroAleatorio);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            director.setLibre(true);
            System.out.println("el director " + director.getNombreCompleto() + " se encuentra disponible");
        }
    }
}
