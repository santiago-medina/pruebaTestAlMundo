
import method.Archivo;
import method.Dispatcher;
import model.Director;
import model.Operador;
import model.Supervisor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static List<Operador> operadores = new ArrayList();
    private static List<Supervisor> supervisores = new ArrayList();
    private static List<Director> directores = new ArrayList();
    private static List<Integer> paqueteLLamadas = new ArrayList();


    public static void main(String[] args) throws IOException {
        Archivo archivo = new Archivo();
        List<String[]> operadoresLista;
        List<String[]> supervisoresLista;
        List<String[]> directoresLista;
        Scanner scanner = new Scanner(System.in);
        Integer numOperadores=-1, numSupervisores=-1, numDirectores=-1, numLLamadas=0;

        /*
        * Mensaje de Bienvenida y captura de entradas de teclado
        * Vamos a capturar cuantos Empleados de cada tipo se encuentran activos en el call center
        * */
        mensajeBienvenida();
        
            while (numOperadores < 0) {
                System.out.print("Numero de Operadores: ");
                try {
                    numOperadores = scanner.nextInt();
                } catch (InputMismatchException e) {
                    scanner.next();
                }
            }

            while (numSupervisores < 0) {
                System.out.print("Numero de Supervisores: ");
                try {
                    numSupervisores = scanner.nextInt();
                } catch (InputMismatchException e) {
                    scanner.next();
                }
            }

            while (numDirectores < 0) {
                System.out.print("Numero de Directores: ");
                try {
                    numDirectores = scanner.nextInt();
                } catch (InputMismatchException e) {
                    scanner.next();
                }
            }

        System.out.println("Configuración exitosa");
        System.out.println("");
        System.out.println("Verificaremos si contamos con el numero de empleados configurados");
        System.out.println("En caso de tener menos se configurara con la maxima cantidad de empleados que se tenga");

        //Se realiza la creación de los empleados
        /*
         * Estos metodos realizan la lectura de los archivos .txt de Operadores,Supervisores y Directores
         * Con el fin de crear un codigo escalable que permita tener un numero variable de empleados
         * El metodo obtenerEmpleados() regresa una lista de empleados (Operadores, Supervisores o Directores)
         * El metodo crearEmpleados() transforma la lista obtenida en objetos para poder manipularlos
         * */
        operadoresLista = archivo.obtenerEmpleados("operador",numOperadores);
        if(!operadoresLista.isEmpty()){
            crearEmpleados(operadoresLista,"operador");
        }
        System.out.println("Se han creado: " + operadores.size() + " Operadores en el sistema");

        supervisoresLista = archivo.obtenerEmpleados("supervisor",numSupervisores);
        if(!supervisoresLista.isEmpty()){
            crearEmpleados(supervisoresLista,"supervisor");
        }
        System.out.println("Se han creado: " + supervisores.size() + " Supervisores en el sistema");

        directoresLista = archivo.obtenerEmpleados("director",numDirectores);
        if(!directoresLista.isEmpty()){
            crearEmpleados(directoresLista,"director");
        }
        System.out.println("Se han creado: " + directores.size() + " Directores en el sistema");

        System.out.println("");
        System.out.println("Se ha realizado la configuración con exito");
        System.out.println("");
        while(numLLamadas<1) {
            System.out.println("¿Cuantas llamadas entrantes deseas simular?: ");
            try {
                numLLamadas = scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.next();
            }
        }

        paqueteLLamadas = obtenerPaqueteLLamadas(numLLamadas);


        if(operadoresLista.isEmpty() && supervisores.isEmpty() && directores.isEmpty()){
            System.out.println("EN ESTE MOMENTO NUESTRO CALL CENTER SE ENCUENTRA CERRADO");
        }
        else{
            Dispatcher dispatcher = new Dispatcher(operadores,supervisores,directores, paqueteLLamadas);
            dispatcher.dispatchCall();
        }

    }

    public static void crearEmpleados(List<String[]> empleados, String cargo){
        if(cargo == "operador"){

            for (String[] a: empleados) {
                Operador operador = new Operador(a[0],a[1],Integer.parseInt(a[2]));
                operadores.add(operador);
            }
        }
        else if(cargo == "supervisor"){
            for (String[] b: empleados) {
                Supervisor supervisor = new Supervisor(b[0],b[1],Integer.parseInt(b[2]));
                supervisores.add(supervisor);
            }
        }
        else if(cargo == "director"){
            for (String[] c: empleados) {
                Director director = new Director(c[0],c[1],Integer.parseInt(c[2]));
                directores.add(director);
            }
        }
    }

    public static void mensajeBienvenida(){
        System.out.println("--------------------------------------------------");
        System.out.println("---  HOLA BIENVENIDO AL CALL CENTER ALMUNDO.COM ----");
        System.out.println("--------------------------------------------------");
        System.out.println("");
        System.out.println("Vamos a realizar la configuración inicial de nuestro simuladore de llamadas");
        System.out.println("");
        System.out.println("Vamos a configurar cuantos empleados estan trabajando el dia de hoy :) ");
    }

    public static List<Integer> obtenerPaqueteLLamadas(Integer numLLamadas){
        List<Integer> paqueteLLamadas = new ArrayList();
        Integer paquetes = 0;
        Integer residuo = 0;

        paquetes = numLLamadas/10;
        residuo = numLLamadas%10;

        if(paquetes == 0){
            paqueteLLamadas.add(numLLamadas);
        }else{
            for (int x = 0; x<paquetes ; x++){
                paqueteLLamadas.add(10);
            }
            if(residuo!=0){
                paqueteLLamadas.add(residuo);
            }
        }

        return paqueteLLamadas;
    }

}
