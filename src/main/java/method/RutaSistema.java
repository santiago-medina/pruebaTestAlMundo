package method;

import java.nio.file.Path;
import java.nio.file.Paths;

public class RutaSistema {

    private String rutaProyecto;
    private String rutaArchivoOperadores;
    private String rutaArchivoSupervisores;
    private String rutaArchivoDirectores;
    private String path = Paths.get("src/main/resources").toAbsolutePath().toString();

    public RutaSistema(){
        rutaProyecto = path;
        rutaArchivoOperadores = path+"/Empleados/Operadores";
        rutaArchivoSupervisores = path+"/Empleados/Supervisores";
        rutaArchivoDirectores = path+"/Empleados/Directores";
    }

    public String getRutaProyecto() {
        return rutaProyecto;
    }

    public String getRutaArchivoOperadores() {
        return rutaArchivoOperadores;
    }

    public String getRutaArchivoSupervisores() {
        return rutaArchivoSupervisores;
    }

    public String getRutaArchivoDirectores() {
        return rutaArchivoDirectores;
    }
}
