package method;

import model.Director;
import model.LLamada;
import model.Operador;
import model.Supervisor;

import java.sql.SQLOutput;
import java.util.List;

public class Dispatcher{

    private static List<Operador> operadores;
    private static List<Supervisor> supervisores;
    private static List<Director> directores;
    private static List<Integer> numLLamadas;
    private Integer paquete;


    public Dispatcher(List<Operador> operadores, List<Supervisor> supervisores, List<Director> directores, List<Integer> numLLamadas){
        this.operadores = operadores;
        this.supervisores = supervisores;
        this.directores = directores;
        this.numLLamadas = numLLamadas;
    }



    public void dispatchCall(){

        for (Integer paqueteLLamadas: numLLamadas) {
            paquete = paqueteLLamadas;
            System.out.println("");
            System.out.println("----- PAQUETE DE LLAMADAS SIMULTANEAS (Maximo 10) -----");
            System.out.println("");
            while(paquete>0) {
                Operador operador = null;
                Supervisor supervisor = null;
                Director director = null;
                if (!operadores.isEmpty()) {
                    operador = obtenerOperadorDisponible(operadores);
                    if(operador != null){
                        llamadaOperador(operador);
                    }else {
                        if (!supervisores.isEmpty()) {
                            supervisor = obtenerSupervisorDisponible(supervisores);
                            if (supervisor != null) {
                                llamadaSupervisor(supervisor);
                            } else{
                                if(!directores.isEmpty()){
                                    director = obtenerDirectorDisponible(directores);
                                    if(director != null){
                                        llamadaDirector(director);
                                    }else{
                                        //System.out.println(" ---------------- EN ESTE MOMENTO NO PODEMOS ATENDER TU LLAMADA, POR FAVOR ESPERA EN LA LINEA");
                                    }
                                }
                            }
                        }else{
                            if(!directores.isEmpty()){
                                director = obtenerDirectorDisponible(directores);
                                if (director != null) {
                                    llamadaDirector(director);
                                } else {
                                    //System.out.println(" ---------------- EN ESTE MOMENTO NO PODEMOS ATENDER TU LLAMADA, POR FAVOR ESPERA EN LA LINEA");
                                }
                            }
                            else{
                                //System.out.println(" ---------------- EN ESTE MOMENTO NO PODEMOS ATENDER TU LLAMADA, POR FAVOR ESPERA EN LA LINEA");
                            }
                        }
                    }

                }else{
                    if(!supervisores.isEmpty()){
                        supervisor = obtenerSupervisorDisponible(supervisores);
                        if (supervisor != null) {
                            llamadaSupervisor(supervisor);
                        } else{
                            if(!directores.isEmpty()){
                                director = obtenerDirectorDisponible(directores);
                                if(director != null){
                                    llamadaDirector(director);
                                }else{
                                    //System.out.println(" ---------------- EN ESTE MOMENTO NO PODEMOS ATENDER TU LLAMADA, POR FAVOR ESPERA EN LA LINEA");
                                }
                            }
                        }
                    }else{
                        if(!directores.isEmpty()){
                            director = obtenerDirectorDisponible(directores);
                            if (director != null) {
                                llamadaDirector(director);
                            }else{
                                //System.out.println(" ---------------- EN ESTE MOMENTO NO PODEMOS ATENDER TU LLAMADA, POR FAVOR ESPERA EN LA LINEA");
                            }
                        }
                    }
                }
            }
            try {
                Thread.sleep(10*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private Operador obtenerOperadorDisponible(List<Operador> operadores){

        for (Operador operador: operadores){
            if(operador.getLibre()){
                operador.setLibre(false);
                return operador;
            }
        }
        return null;
    }

    private Supervisor obtenerSupervisorDisponible(List<Supervisor> supervisores){
        for (Supervisor supervisor: supervisores){
            if(supervisor.getLibre()){
                supervisor.setLibre(false);
                return supervisor;
            }
        }
        return null;
    }

    private Director obtenerDirectorDisponible(List<Director> directores){
        for (Director director: directores){
            if(director.getLibre()){
                director.setLibre(false);
                return director;
            }
        }
        return null;
    }

    private void llamadaOperador(Operador operador){
        LLamada llamadaOperador = new LLamada(operador);
        llamadaOperador.start();
        paquete--;
    }

    private void llamadaSupervisor(Supervisor supervisor){
        LLamada llamadaSupervisor = new LLamada(supervisor);
        llamadaSupervisor.start();
        paquete--;
    }

    private void llamadaDirector(Director director){
        LLamada llamadaDirector = new LLamada(director);
        llamadaDirector.start();
        paquete--;
    }

}
