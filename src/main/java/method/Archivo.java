package method;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Archivo {

    private RutaSistema rutaSistema = new RutaSistema();
    private Reporte reporte = new Reporte();

    public Archivo(){

    }

    public List<String[]> obtenerEmpleados(String empleado, Integer numeroEmpleados){

        List<String[]> datos = new ArrayList();

        try {

            if(numeroEmpleados>0) {
                if (empleado == "operador") {
                    datos = obtenerDatosArchivo(rutaSistema.getRutaArchivoOperadores(), empleado, numeroEmpleados);
                } else if (empleado == "supervisor") {
                    datos = obtenerDatosArchivo(rutaSistema.getRutaArchivoSupervisores(), empleado, numeroEmpleados);
                } else if (empleado == "director") {
                    datos = obtenerDatosArchivo(rutaSistema.getRutaArchivoDirectores(), empleado, numeroEmpleados);
                } else {
                    datos.clear();
                }
            }else{
                datos.clear();
            }

        } catch (IOException e) {
            datos.clear();
        }
        return datos;
    }



    public List<String[]> obtenerDatosArchivo(String ruta, String empleado, Integer numeroEmpleados) throws IOException {
        String cadena;
        Integer n = 0, numEmpleados = numeroEmpleados;
        List<String[]> datos = new ArrayList();
        List<String> errores = new ArrayList();
        File f = new File(ruta);
        if (f.exists()){
            FileReader fr = new FileReader(f);
            BufferedReader b = new BufferedReader(fr);
            while((cadena = b.readLine())!=null) {
                if(cadena.length()!=0 && numeroEmpleados>0){
                    String[] parts = cadena.split(",");
                    if(parts.length == 3){
                        datos.add(parts);
                        numeroEmpleados--;
                    }
                    else{
                        String error = "La linea: " + n + " no se encuentra en el formato requerido";
                        errores.add(error);
                    }

                }else{
                    String error = "La linea: " + n + " se encuentra vacia";
                    errores.add(error);
                }
                n++;
            }
            b.close();
        }
        else{
            String error = "El archivo " + ruta + " no existe";
            errores.add(error);
        }

        if (errores.size()!=0){
            reporte.generarReporteErrores(errores,empleado);
        }
        return  datos;
    }

}
