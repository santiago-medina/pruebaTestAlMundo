import method.Dispatcher;
import model.Director;
import model.Operador;
import model.Supervisor;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CallCenterTest {

    @Test
    public void callCenterTest(){
        /*
        * Se van a crear 10 empleados:
        * 6 Operadores
        * 3 Supervisores
        * 1 Director
        * */
        Operador operador1 = new Operador("Santiago","Medina",102030);
        Operador operador2 = new Operador("Santiago","Medina",102030);
        Operador operador3 = new Operador("Santiago","Medina",102030);
        Operador operador4 = new Operador("Santiago","Medina",102030);
        Operador operador5 = new Operador("Santiago","Medina",102030);
        Operador operador6 = new Operador("Santiago","Medina",102030);
        List<Operador> operadores = new ArrayList();
        operadores.add(operador1);
        operadores.add(operador2);
        operadores.add(operador3);
        operadores.add(operador4);
        operadores.add(operador5);
        operadores.add(operador6);

        Supervisor supervisor1 = new Supervisor("Pepito","Perez",203040);
        Supervisor supervisor2 = new Supervisor("Pepito","Perez",203040);
        Supervisor supervisor3 = new Supervisor("Pepito","Perez",203040);
        List<Supervisor> supervisores = new ArrayList();
        supervisores.add(supervisor1);
        supervisores.add(supervisor2);
        supervisores.add(supervisor3);

        Director director1 = new Director("Juanito","Perez",405060);
        List<Director> directores = new ArrayList();
        directores.add(director1);

        /*
        * Se genera la lista de llamadas entrantes
        * Como maximo son 10 llamadas concurrentes, segun la logica se creo una lista de Integer empaquetando
        * las llamadas en grupos de 10
        * */

        List<Integer> paqueteLLamadas = new ArrayList();
        paqueteLLamadas.add(10);

        Dispatcher dispatcher = new Dispatcher(operadores,supervisores,directores, paqueteLLamadas);
        dispatcher.dispatchCall();

    }


}
