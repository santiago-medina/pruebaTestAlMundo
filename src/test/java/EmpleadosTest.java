import model.Director;
import model.Operador;
import model.Supervisor;

public class EmpleadosTest {

    @org.testng.annotations.Test
    public void probarCreacionOperador(){
        Operador operador = new Operador("Santiago","Medina",102030);
        assert ("Santiago" == operador.getNombre());
        assert ("Medina" == operador.getApellido());
        assert (102030 == operador.getCedula());
        assert ("operador" == operador.getTipoEmpleado());
    }

    @org.testng.annotations.Test
    public void probarCreacionSupervisor(){
        Supervisor supervisor = new Supervisor("Santiago","Medina",102030);
        assert ("Santiago" == supervisor.getNombre());
        assert ("Medina" == supervisor.getApellido());
        assert (102030 == supervisor.getCedula());
        assert ("supervisor" == supervisor.getTipoEmpleado());
    }

    @org.testng.annotations.Test
    public void probarCreacionDirector(){
        Director director = new Director("Santiago","Medina",102030);
        assert ("Santiago" == director.getNombre());
        assert ("Medina" == director.getApellido());
        assert (102030 == director.getCedula());
        assert ("director" == director.getTipoEmpleado());
    }



}
