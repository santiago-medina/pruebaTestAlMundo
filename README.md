# Prueba Técnica AlMundo
A continuación se agrega la documentación necesaria para poder ejecutar nuestro proyecto Java Orientado a Objetos.

## Problema a solucionar
*Existe un call center donde hay 3 tipos de empleados: operador, supervisor y director. El proceso de la atención de una llamada telefónica en primera instancia debe ser atendida por un operador, si no hay ninguno libre debe ser atendida por un supervisor, y de no haber tampoco supervisores libres debe ser atendida por un director. El Call Center solo tiene la capacidad de procesar 10 llamadas simultaneas, donde cada llamada puede durar entre 5 a 10 segundos.*

## Construcción
De este planteamiento tenemos por tanto 3 clases princiales:
- `Operador.java`
- `Supervisor,java`
- `Director.java`

Para este ejemplo practico, las 3 clases poseen los mismos atributos:

```java
String nombre;
String apellido;
Integer cedula;
String tipoEmpleado;
Boolean libre;
```
por tanto, para utilizar los conceptos de POO, se crea una clase Padre denominada `Empleado.java`, de la cual heredan las 3 clases anteriores.

Para la generación instantanea de Operadores, Supervisores y Directores, se crearon archivos de carga en formato `.txt`, ubicados en la carpeta `resources/Empleados/` donde se encuentran los archivos de carga:
- `Operadores.txt`
- `Supervisores.txt`
- `Directores.txt`

![Imagen Archivos de Carga](https://scontent.fbog3-1.fna.fbcdn.net/v/t1.0-9/31485379_10156305597172974_7789937580491211546_n.jpg?_nc_cat=0&_nc_eui2=v1%3AAeFhrxEzKULSyCfNCdEixOYFAEYiLjFoIc0R84zclJ0WuQY7qU1OgDarbSU-Ep50z6K7KsFzmXV6IRNv-MckiEA27bBo_OA59g2z21bCZhhCDg&oh=a47bee7eb5b04c4e8783b865241a2e39&oe=5B5E8A5E)

De esta manera se puede realizar simulaciones del Call Center con diferentes configuraciones de Empleados.

Para lograr este fin, se crearon las clases:
- `RutaSistema.java` : Encargada de capturar la ruta de los archivos .txt y transformarlos a `String.`
- `Archivo.java` : Encargada de realizar la lectura de estos archivos `.txt`convirtiendo esa información en variables de tipo `List<String>`, las cuales posteriormente se convertiran en Objetos de tipo `Empleado` , `Supervisor` o `Director`, usando la siguiente logica de programación:

```Java
Archivo archivo = new Archivo();
int numeroOperadores = 5;
/*Obtengo una lista de String con los datos de 5 empleados*/
List<String> operadoresLista = archivo.obtenerEmpleados("operador",numeroOperadores);

for(String[] a : operadoresLista){
  Operador operador = new Operador(a[0],a[1],Integer.parseInt(a[2]));
}

```
## Manejo de llamadas
Como se especifico en el planteamiento del problema, el Call Center solo tiene la capacidad de procesar 10 llamadas simultaneas, por tanto se realiazaron las siguientes concideraciones para manejar dos excepciones:
- ¿Que pasa cuando entran más de 10 llamadas concurrentes?
- ¿Que pasa cuando no hay empleados libres?

#### Manejo de más de 10 llamadas
Tomando la restricción de que nuestro Call Center solamente puede manejar 10 llamadas simultaneas, se decidio encapsular el numero de llamadas en paquetes de 10 por medio de una variable `List<Integer>` la cual fuera pasando a nuestro Call Center paquetes de 10 llamadas hasta gestionar el total de llamadas.

Para la creación de este `List<Integer>` se realizo el siguiente codigo:

```java
Integer numLLamadas = 43;
List<Integer> paqueteLLamadas = new ArrayList();
        Integer paquetes = 0;
        Integer residuo = 0;

        paquetes = numLLamadas/10;
        residuo = numLLamadas%10;

        if(paquetes == 0){
            paqueteLLamadas.add(numLLamadas);
        }else{
            for (int x = 0; x<paquetes ; x++){
                paqueteLLamadas.add(10);
            }
            if(residuo!=0){
                paqueteLLamadas.add(residuo);
            }
        }
```
De esta manera para un total de 43 llamadas tendremos en nuestro `List<Integer> paqueteLLamadas` : `[10],[10],[10],[10],[3]`.

Cada uno de estos paquetes se va entregando a la Clase `Dispatcher.java` que se encarga de enviar las llamadas a los empleados deacuerdo a su disponibilidad.


### Manejo de llamada en espera
Para el caso de que se manden las 10 llamadas simultaneas y no se encuentren Empleados que puedan atender la llamada, se creo un ciclo `while` cuya funcion es verificar que cada llamada de los paquetes de llamada enviados sean gestionados, la logica de programación realizada fue la siguiente:

```Java
Integer paqueteEnProceso;
for(Integer paquete: paqueteLLamadas){
  paqueteEnProceso = paquete;
  while(paqueteEnProceso>0){
    /*
      Logica para asignar llamada a los diferentes Empleados
    */
    if(seProcesoLaLLamada){
      paqueteEnProceso--;
    }
  }
}
```

### Test Unitarios realizados
Los Test Unitarios del proyecto se realizaron en la carpeta `src/test/java/` presentes en las clases:
- `EmpleadosTest.java` : Encargada de verificar la correcta creación de los Empleados.
- `CallCenterTest.java` : Encargada de crear 6 Operadores, 3 Supervisores y 1 Director, ademas de generar un trafico de 10 llamadas, verificando su correcta ejecución.

![Ubicación test unitario](https://scontent.fbog3-1.fna.fbcdn.net/v/t1.0-9/31676628_10156305656642974_7319875336946501298_n.jpg?_nc_cat=0&_nc_eui2=v1%3AAeF4GChBDp3q9HU4GtZc30zlC_onc6F6tKvmPltt7elKj5PmcWuu4GYHqARNGhKFNkqXEUVkDmrgjJ_4ZPipJIDN7ArePBQrKK7cjJncfbSnAg&oh=2e80adb239e5d6f0c55a00f2c8b8f62d&oe=5B994EDE)
